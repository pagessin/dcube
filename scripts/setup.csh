# enviroment setup for dcube.py running for (t)csh shell
#
# Aug 16, 2007 - cibak - switch to root 5.16/00
# Oct 23, 2007 - cibak - switch to root 5.17/04
# Apr 10, 2008 - cibak - switch to root 5.19/02 and python 2.5
# May 27, 2020 - adye - ALRB setup. Don't add ROOT, so can keep user's choice.

if (! $?ATLAS_DCUBE_DIR) then
  setenv ATLAS_DCUBE_DIR /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/dcube/current
endif
setenv PATH "${ATLAS_DCUBE_DIR}/bin:${PATH}"
