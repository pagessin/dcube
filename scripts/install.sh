#!/bin/bash
# Install DCube by creating symlinks to find files in the expected places.

cd "$(dirname "$0")/.."
mkdir -p bin
ln -nfs ../scripts/art-dcube ../python/dcube.py ../python/dcubeConvert.py bin/
ln -nfs . DCubeClient
