DCube
=====

[DCube](https://twiki.cern.ch/twiki/bin/view/AtlasComputing/DCube) can be used to produce comparison plots of an ART job's output with a fixed reference, or a previous nightly. The results can be selected for automatic download to EOS and viewable as web pages. You will need the results in a single root file, containing 1D/2D histograms (`TH1`), 1D/2D profile plots (`TProfile`), or graphs (`TGraph`, `TEfficiency`).

To run DCube from your job:

```
$ATLAS_LOCAL_ROOT/dcube/current/DCubeClient/python/dcube.py -p -x outdir -c config.xml -r reference.root results.root
```

`dcube.py` can be added to your PATH with `lsetup dcube`. DCube uses [PyROOT](https://root.cern/manual/python/) (Python2 or 3), so you may need to set up ROOT seprately - or use the version that comes with Athena (`asetup`). ART jobs have all this set up already for you.

Running DCube from ART jobs
---------------------------

Here is an example from [InDetPhysValMonitoring's test_ttbarPU40_reco.sh](https://gitlab.cern.ch/atlas/athena/-/blob/master/InnerDetector/InDetValidation/InDetPhysValMonitoring/test/test_ttbarPU40_reco.sh):

```
# art-output: dcube*
...
dcube.py \
   -p -x dcube \
   -c /cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/InDetPhysValMonitoring/dcube/config/IDPVMPlots_R22.xml \
   -r /cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/InDetPhysValMonitoring/ReferenceHistograms/physval_ttbarPU40_reco_r22.root \
   physval.hist.root
echo "art-result: $? plots"
```

The `# art-output: dcube*` header line ensures that the `dcube` output directory (and any others matching `dcube*`) is downloaded to EOS, so it can be accessed from the web. `echo "art-result: $? plots"` records the DCube comparison results so they can be shown on the ART display.

If the fixed reference is used by only one ART test, then it might be more convenient to specify the reference file name in the `config.xml` file (`<reference file="reference.root">`). In this case the `-r reference.root` option can be omitted from the `dcube.py` command. This allows the reference file to be updated in CVMFS without needing a new GitLab merge request.

That example compared against a fixed reference. To compare against a previous nightly, the previous nightly's results should be downloaded by the test job script:

```
art.py download --user=artprod --dst=subdir "$ArtPackage" "$ArtJobName"
```

The root file in this download can then be specified as the reference file name, eg.

```
art.py download --user=artprod --dst=last_results "$ArtPackage" "$ArtJobName"
dcube.py \
   -p -x dcube_last \
   -c /cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/InDetPhysValMonitoring/dcube/config/IDPVMPlots_R22.xml \
   -r last_results/physval.hist.root \
   physval.hist.root
echo "art-result: $? plots"
```

DCube setup
-----------

To setup DCube for a new ART test, you need to create an example results root file (`results.root`) and a DCube configuration XML file (`config.xml`). The results root file can be downloaded from an existing ART job, or created by [running the ART job locally](https://twiki.cern.ch/twiki/bin/view/AtlasComputing/ART#Running_jobs_locally). This root file can be [copied to CVMFS](https://twiki.cern.ch/twiki/bin/view/AtlasComputing/ART#Inputs_read_from_CVMFS) and used as your first fixed reference file.

A first draft of the DCube configuration XML file can be generated from the `results.root`:

```
lsetup dcube
rm -f config.xml      # create a new config file; don't try to update an old one.
dcube.py -g -c config.xml -r reference.root -t TESTS
```

Note the use of `-g`, without `-p -x outdir results.root`. This will create a new `config.xml`, which you can then edit. There are probably many more histograms in your result file than you want to look at every night, so you can remove all the extraneous ones from the `config.xml`. It is also useful to specify the location of the reference file in CVMFS (<reference file="reference.root"> to provide a default for later), titles for the monitored (`<test_desc>`) and reference samples (`<ref_desc>`), and limits for how large a difference in p-value counts as a warning or failure (`<plimit fail="0.75" warn="0.95"/>`)

It's worth specifying the `TESTS` option on the dcube.py command line, since it is added to every histogram listing in `config.xml`. `TESTS` specifies a comma separated list of statistical tests to be run: `KS,chi2,bbb,meany`. Use `all` to run all tests.

  * `KS`: Kolmogorov-Smirnov test (default)
  * `chi2`: Pearson's χ2 test
  * `bbb`: bin-by-bin test (fraction of non-empty bins with any difference)
  * `meany`: rather arbitrary test on the mean of y (for `TProfile` objects only)
  * `all`: run all tests

See the [original DCube documentation](https://twiki.cern.ch/twiki/bin/view/AtlasComputing/DCube) for more details.
