#!/bin/env python

# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

import sys
try:
    from             DCubeApp import DCubeApp
except ImportError as import_error:
    try:
        from DCubeClient.DCubeApp import DCubeApp
    except ImportError:
        raise import_error

if __name__ == "__main__":
    ## to run ROOT in batch mode
    sys.argv.insert(1,"-b")
    DCubeApp( )
