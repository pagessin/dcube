#!/bin/env python

# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
##
# @file DCubeClient/python/DCubeTester.py
# @author Krzysztof Daniel Ciba (Krzyszotf.Ciba@NOSPAMgmail.com)
# @brief implementation of DCubeTester and test_DCubeTester classes

import sys
import ctypes
from DCubeUtils import DCubeObject, DCubeException
from DCubePlotter import DCubePlotter
import ROOT
import unittest
import math
from array import array
from collections import Counter
import warnings

##
# @class DCubeTester
# @author Krzysztof Daniel Ciba (Krzysztof.Ciba@NOSPAMgmail.com)
# @brief perform statictics tests and plotting
class DCubeTester( DCubeObject ):

    ## XML DOM Document instance
    xmldoc = None

    ## XML DOM Element instance
    node = None

    ## handle for monitored root object
    mon = None

    ## handle for reference root object
    ref = None

    ## errors counters
    errors = { }

    ## intarnal histogram counter
    __nbObjs = 0

    ## statistics summary table
    sumTable = { "KS"   : { "OK"   : 0,
                            "WARN" : 0,
                            "FAIL" : 0 },
                 "chi2" : { "OK"   : 0,
                            "WARN" : 0,
                            "FAIL" : 0 },
                 "bbb"  : { "OK"   : 0,
                            "WARN" : 0,
                            "FAIL" : 0 },
                 "meanY": { "OK"   : 0,
                            "WARN" : 0,
                            "FAIL" : 0 }}


    ## chi2 error communicates
    chi2igood = { 0 : "no problems",
                  1 : "there is bin in mon hist with low then 1 exp number of event",
                  2 : "there is bin in ref hist with low then 1 exp number of event",
                  3 : "there are bins in both histograms with less than 1 event" }


    ## summary status
    __status = ""

    ## c'tor
    # @param self "Me, myself and Irene"
    # @param xmldoc XML DOM Document instance
    def __init__( self, xmldoc, parsed ):

        super( DCubeTester, self ).__init__( self )
        self.xmldoc = xmldoc
        self.opts, self.args = parsed
        self.tests = { "KS"   : self.__testKS,
                       "bbb"  : self.__testBBB,
                       "chi2" : self.__testChi2,
                       "meany": self.__testMeanY }


        if ( self.opts.makeplots ):
            self.info("will produce plot files")
            self.__plotter = DCubePlotter( xmldoc, parsed )
        else:
            self.warn("making of plots disabled")

    ##
    # @param self "Me, myself and Irene"
    # @param node DOM XML  node
    # @param mon monitored ROOT object
    # @param ref reference ROOT object
    # @return modified XML node
    def test(self, node, mon=None, ref=None, monPath=None, refPath=None):
        self.__nbObjs += 1

        self.node = self.mon = self.ref = None

        self.node = node
        self.parent = self.node.parentNode

        self.mon = mon
        self.ref = ref

        self.monPath = monPath if monPath else self.node.getAttribute("name")
        self.refPath = refPath if refPath else self.monPath

        # mon exists?
        if not self.mon:
            status = "FAIL;monitored not found"
            self.node.setAttribute("status", status)
            if status not in self.errors.keys():
                self.errors[status] = 1
            else:
                self.errors[status] = self.errors[status] + 1
            self.error("monitored object '%s' not found!" % self.monPath)
            return "FAIL"

        if self.mon.Class().GetName() == "TEfficiency":
            # Replace with TGraphAsymmErrors (1D) or TH2F (2D) for stats and plotting
            self.mon = self.__TEfficiency2TGraph (self.mon)
            self.ref = self.__TEfficiency2TGraph (self.ref)

        cl = self.mon.Class().GetName()
        isHist = (cl[:2] in ("TH", "TP"))

        # dimension OK?
        if (isHist and self.mon.GetDimension() > 2):
            status = "FAIL;unsupported object, dimension bigger than 2"
            self.node.setAttribute("status", status)
            if status not in self.errors.keys():
                self.errors[status] = 1
            else:
                self.errors[status] = self.errors[status] + 1
            self.error("unsuported object '%s' found" % self.monPath)
            return "FAIL"

        # reference exists? Skip this test if there is no reference root file.
        if ( not self.ref and self.ref != False ):
            self.warn( "reference object '%s' not found!" % self.refPath )
            status = "WARN;reference histogram not found"
            self.node.setAttribute( "status", status )
            if ( status not in self.errors.keys() ):
                self.errors[ status ] = 1
            else:
                self.errors[ status ] = self.errors[ status ] + 1

        if ( self.mon and self.ref ):
            # same class?
            monClassName = self.mon.Class().GetName()
            refClassName = self.ref.Class().GetName()
            if ( monClassName != refClassName ):
                status = "FAIL;different root types"
                self.node.setAttribute( "status", status + " mon=%s ref=%s" % ( monClassName,
                                                                                refClassName ) )
                if ( status not in self.errors.keys() ):
                    self.errors[ status ] = 1
                else:
                    self.errors[ status ] = self.errors[ status ] + 1
                self.error("different types for mon and ref '%s' objects!" % self.monPath)
                return "FAIL"


        if ( isHist ):
            self.monBins = [ self.mon.GetNbinsX(), self.mon.GetNbinsY() ]
        if ( isHist and self.ref ):
            self.refBins = [ self.ref.GetNbinsX(), self.ref.GetNbinsY() ]

        if ( isHist and self.mon and self.ref ):
            # same binnig?
            monBins = [ "%s=%d" % ( x, y) for ( x, y) in zip(["x", "y"], self.monBins ) ]
            refBins = [ "%s=%d" % ( x, y) for ( x, y) in zip(["x", "y"], self.refBins ) ]
            diffBins = [ "mon %s ref %s" % (x, y) for (x,y) in zip(monBins,refBins) if x != y ]
            if ( diffBins  ):
                status = "FAIL;different binning"
                self.node.setAttribute( "status", status + " %s" % " ".join(diffBins) )
                if ( status not in self.errors.keys() ):
                    self.errors[ status ] = 1
                else:
                    self.errors[ status ] = self.errors[ status ] + 1
                self.error("different binning for mon and ref '%s' objects!" % self.monPath)
                return "FAIL"

        status = "OK"
        if ( isHist ):
            status = self.__grabHistogramStat()
        else:
            status = self.__grabGraphStat()

        plotStatus = "OK"
        if ( self.opts.makeplots ):
            plotStatus = self.__makePlots()
        else:
            plotStatus = "WARN;plots not reqested"
        self.node.setAttribute( "plots", plotStatus)

        return status


    ## grab statistic info for TGraphXXX
    # @param self "Me, myself and Irene"
    def __grabGraphStat( self ):
        if self.mon.GetHistogram().GetDimension() == 1: axis = 2  # change to y-mean and y-RMS (10/7/2017)
        else:                                           axis = 1  # TGraph2D probably not supported anyway

        statNode = self.xmldoc.createElement( "stat" )
        self.node.appendChild( statNode )

        nbPointsNode = self.xmldoc.createElement( "points" )
        nbPointsCData = self.xmldoc.createTextNode( "%d" % self.mon.GetN() )
        if ( self.ref ):
            nbPointsNode.setAttribute( "ref" , "%d" % self.ref.GetN() )
        nbPointsNode.appendChild( nbPointsCData )

        meanNode = self.xmldoc.createElement( "mean" )
        meanCData = self.xmldoc.createTextNode( "%4.3f" % self.mon.GetMean(axis) )
        if ( self.ref ):
            meanNode.setAttribute( "ref" , "%4.3f" % self.ref.GetMean(axis) )
        meanNode.appendChild( meanCData )

        rmsNode = self.xmldoc.createElement( "rms" )
        rmsCData = self.xmldoc.createTextNode( "%4.3f" % self.mon.GetRMS(axis) )
        if ( self.ref ):
            rmsNode.setAttribute( "ref" , "%4.3f" % self.ref.GetRMS(axis) )
        rmsNode.appendChild( rmsCData )

        statNode.appendChild( nbPointsNode )
        statNode.appendChild( meanNode )
        statNode.appendChild( rmsNode )

        return self.__runTests (statNode)



    ## grab basic statistics - nb of entries, mean and RMS
    # @param self "Me, myself and Irene"
    def __grabHistogramStat( self ):

        statNode = self.xmldoc.createElement( "stat" )

        self.node.appendChild( statNode )

        entriesNode = self.xmldoc.createElement("entries")
        entriesCDataNode = self.xmldoc.createTextNode( str( self.mon.GetEntries() ) )
        if ( self.ref ):
            entriesNode.setAttribute( "ref", str( self.ref.GetEntries() ) )
        entriesNode.appendChild( entriesCDataNode )

        statNode.appendChild( entriesNode )

        dim = self.mon.GetDimension()

        ### To dump MeanY values
        if ( "TProfile" in self.mon.Class().GetName() ): dim = 2

        # store under and overflows
        monU = []
        monO = []
        refU = []
        refO = []

        #store underflows and overflows to check is some of them are inf or nan
        vals = []

        if ( dim == 1 ):
            monU.append( self.mon.GetBinContent(0) )
            vals.append( self.mon.GetBinContent(0) )
            monO.append( self.mon.GetBinContent( self.mon.GetNbinsX()+1 ) )
            vals.append( self.mon.GetBinContent( self.mon.GetNbinsX()+1 ) )
            if ( self.ref ):
                refU.append( self.ref.GetBinContent(0) )
                vals.append( self.ref.GetBinContent(0) )
                refO.append( self.ref.GetBinContent( self.ref.GetNbinsX()+1 ) )
                vals.append( self.ref.GetBinContent( self.ref.GetNbinsX()+1 ) )
        elif ( dim == 2 ):
            monU.append( self.mon.GetBinContent( 0, 0 ) )
            vals.append( self.mon.GetBinContent( 0, 0 ) )
            monO.append( self.mon.GetBinContent( self.mon.GetNbinsX()+1, 0 ) )
            vals.append( self.mon.GetBinContent( self.mon.GetNbinsX()+1, 0 ) )
            monU.append( self.mon.GetBinContent( 0,  self.mon.GetNbinsY()+1) )
            vals.append( self.mon.GetBinContent( 0,  self.mon.GetNbinsY()+1) )
            monO.append( self.mon.GetBinContent( self.mon.GetNbinsX()+1, self.mon.GetNbinsY()+1 ) )
            vals.append( self.mon.GetBinContent( self.mon.GetNbinsX()+1, self.mon.GetNbinsY()+1 ) )
            if ( self.ref ):
                refU.append( self.ref.GetBinContent( 0, 0 ) )
                vals.append( self.ref.GetBinContent( 0, 0 ) )
                refO.append( self.ref.GetBinContent( self.ref.GetNbinsX()+1, 0 ) )
                vals.append( self.ref.GetBinContent( self.ref.GetNbinsX()+1, 0 ) )
                refU.append( self.ref.GetBinContent( 0,  self.ref.GetNbinsY()+1) )
                vals.append( self.ref.GetBinContent( 0,  self.ref.GetNbinsY()+1) )
                refO.append( self.ref.GetBinContent( self.ref.GetNbinsX()+1, self.ref.GetNbinsY()+1 ) )
                vals.append( self.ref.GetBinContent( self.ref.GetNbinsX()+1, self.ref.GetNbinsY()+1 ) )

        #check if some values are inf or nan
        if ( self.__checkValues(vals) != "OK" ):
            self.warn( "some values in histogram '%s' are infinity or NaN, skipping!" % self.monPath )
            self.node.setAttribute( "status", "FAIL;monitored or reference histogram contains inf or NaN values")
            return "FAIL"

        underflowNode = self.xmldoc.createElement( "underflow" )
        underflowCData = self.xmldoc.createTextNode( "%d" % sum( monU ) )
        if ( self.ref ):
            underflowNode.setAttribute( "ref", "%d" % sum( refU) )
        underflowNode.appendChild( underflowCData )

        overflowNode = self.xmldoc.createElement( "overflow" )
        overflowCData = self.xmldoc.createTextNode( "%d" % sum( monO ) )
        if ( self.ref ):
            overflowNode.setAttribute( "ref", "%d" % sum( refO ) )
        overflowNode.appendChild( overflowCData )

        statNode.appendChild( underflowNode )
        statNode.appendChild( overflowNode )

        dimName = [ "x", "y", "z" ]
        for i in range( 1, dim+1 ):
            self.debug( "gathering statistics along %s axis" % dimName[i-1] )

            dimNode = self.xmldoc.createElement( "dim" )
            dimNode.setAttribute( "name", dimName[i-1] )
            dimNode.setAttribute( "bins", "%d" % self.monBins[i-1])

            underflowNode = self.xmldoc.createElement( "underflow" )
            underflowCData  = self.xmldoc.createTextNode( "%d" % monU[i-1] )
            if ( self.ref ):
                underflowNode.setAttribute( "ref", "%d" % refU[i-1] )
            underflowNode.appendChild( underflowCData )

            dimNode.appendChild( underflowNode )

            overflowNode = self.xmldoc.createElement( "overflow" )
            overflowCData  = self.xmldoc.createTextNode( "%d" % monO[i-1] )
            if ( self.ref ):
                overflowNode.setAttribute( "ref", "%d" % refO[i-1] )
            overflowNode.appendChild( overflowCData )

            dimNode.appendChild( overflowNode )

            meanNode = self.xmldoc.createElement( "mean" )
            if ( self.ref ):
                meanNode.setAttribute( "ref", "%3.2e" % self.ref.GetMean(i) )

            meanCData = self.xmldoc.createTextNode( "%3.2e" % self.mon.GetMean( i ) )
            meanNode.appendChild( meanCData )

            meanErrorNode = self.xmldoc.createElement( "mean_unc" )
            if ( self.ref ):
                meanErrorNode.setAttribute( "ref", "%3.2e" % self.ref.GetMeanError(i) )

            meanErrorCData = self.xmldoc.createTextNode( "%3.2e" % self.mon.GetMeanError( i ) )
            meanErrorNode.appendChild( meanErrorCData )

            rmsNode = self.xmldoc.createElement( "rms" )
            if ( self.ref ):
                rmsNode.setAttribute( "ref", "%3.2e" % self.ref.GetRMS(i) )
            rmsCData = self.xmldoc.createTextNode( "%3.2e" % self.mon.GetRMS(i) )
            rmsNode.appendChild( rmsCData )

            rmsErrorNode = self.xmldoc.createElement( "rms_unc" )
            if ( self.ref ):
                rmsErrorNode.setAttribute( "ref", "%3.2e" % self.ref.GetRMSError(i) )
            rmsErrorCData = self.xmldoc.createTextNode( "%3.2e" % self.mon.GetRMSError(i))
            rmsErrorNode.appendChild( rmsErrorCData )

            dimNode.appendChild( meanNode )
            dimNode.appendChild( meanErrorNode )
            dimNode.appendChild( rmsNode )
            dimNode.appendChild( rmsErrorNode )

            statNode.appendChild( dimNode )

        return self.__runTests (statNode)

    ## run all tests
    # @param self "Me, myself and Irene"
    # @param statNode <stat> element
    def __runTests( self, statNode ):

        status = [ ]

        tests = [t for t in self.node.getAttribute('tests').strip().split(',') if t]


        if ( self.node.getAttribute('pwarn') ):
            try:
                pwarn = float ( self.node.getAttribute('pwarn').strip() )
            except TypeError as value:
                raise DCubeException( "histogram %s pvalue limit for WARN is NAN" % self.monPath )
            self.info("overriding pvalue limits for histogram %s: new WARN limit is pwarn=%s" % ( self.monPath, pwarn ) )
        else:
            pwarn = self.opts.pwarn

        if ( self.node.getAttribute('pfail') ):
            try:
                pfail = float ( self.node.getAttribute('pfail').strip() )
            except TypeError as value:
                raise DCubeException( "histogram %s pvalue limit for FAIL is NAN" % self.monPath )
            self.info("overriding pvalue limits for histogram %s: new FAIL limit is pfail=%s" % ( self.monPath, pfail ) )
        else:
            pfail = self.opts.pfail



        if ( "all" in tests ): tests = [ "KS", "chi2", "bbb", "meany" ]

        if ( all ( (self.mon, self.ref) ) ):
            if ( "TProfile" not in self.mon.ClassName() and "meany" in tests ):
                tests.remove("meany")
            if tests:
                self.monhist = self.__2hist (self.mon, [['reference', self.ref]], "monitored %s '%s'" % (self.mon.ClassName(), self.monPath))
                self.refhist = self.__2hist (self.ref, [['monitored', self.mon]], "reference %s '%s'" % (self.ref.ClassName(), self.refPath))
                for test in tests:
                    status.append( self.tests[ test ].__call__( statNode, pwarn, pfail ) )

        statusAttr = "OK"
        if ( "FAIL" in status ): statusAttr = "FAIL"
        elif ( "WARN" in status ): statusAttr = "WARN"

        self.node.setAttribute( "stest", statusAttr )

        return statusAttr

    ## perform @f$\chi^2@f$ test
    # @param self "Me, myself and Irene"
    # @param statNode <stat> element
    def __testChi2( self, statNode, pwarn, pfail ):

        chi2 = 0.0
        igood = 0
        ndf = 0


        nbBins,nbBinsSame,nbBinsDiff,nMon,nRef = self.__testEqual()
        self.debug("*** Pearson's chi2 *** all non-empty bins=%d all equal non-empty bins=%d" % ( nbBins, nbBinsSame ) )

        pval = 1.0
        if nMon==0 and nRef==0:
            self.warn( "*** Pearson's chi2 *** both '%s' histograms are empty!" % self.monPath )
        elif nbBinsDiff==0:
            self.debug( "*** Pearson's chi2 *** both '%s' histograms are equal!" % self.monPath )
        elif not nMon:
            self.warn( "*** Pearson's chi2 *** monitored '%s' histogram is empty!" % self.monPath )
            pval = 0.0
        elif not nRef:
            self.warn( "*** Pearson's chi2 *** reference '%s' histogram is empty!" % self.refPath )
            pval = 0.0
        else:
            chi2 = ctypes.c_double( chi2 )
            igood = ctypes.c_int( igood )
            ndf = ctypes.c_int( ndf )
            pval = self.monhist.Chi2TestX( self.refhist, chi2, ndf, igood, "UUDNORM")
            chi2 = chi2.value
            ndf = ndf.value
            igood = igood.value

            self.debug( "*** Pearson's chi2 *** (UUDNORM) p-value= %4.3f" % pval )
            ig = "*** Pearson's chi2 *** igood = %d, %s for '%s'" % ( igood, self.chi2igood[igood], self.monPath )
            if ( igood == 0 ):
                self.debug( ig )
            else:
                self.warn( ig )

        status = self.__getTestStatus( pval, pwarn, pfail )
        self.sumTable["chi2"][status] = self.sumTable["chi2"][status] + 1

        if ( ndf != 0 ):
            self.info( "*** Pearson's chi2 *** chi2 (/ndf) = %4.3f (/%d = %4.3f) status=%s" % ( chi2,
                                                                                                ndf,
                                                                                                chi2/ndf,
                                                                                                status ) )
        else:
            self.info( "*** Pearson's chi2 *** chi2 = %4.3f ndf = %d status=%s" % ( chi2,
                                                                                    ndf,
                                                                                    status ) )

        pValueNode = self.xmldoc.createElement( "pvalue" )
        pValueCData = self.xmldoc.createTextNode( "%4.3f" % pval )

        pValueNode.setAttribute( "test", "chi2" )
        pValueNode.setAttribute( "status", status )

        counter = self.parent.getAttribute( "chi2" + status )
        if ( not counter ): counter = "0"
        self.parent.setAttribute( "chi2" + status, "%d" % ( int(counter) + 1 )  )

        pValueNode.appendChild( pValueCData )

        statNode.appendChild( pValueNode )


        return status

    ## perform Kolmogorov-Smirnoff test
    # @param self "Me, myself and Irene"
    # @param statNode <stat> element
    def __testKS( self, statNode, pwarn, pfail ):

        nbBins,nbBinsSame,nbBinsDiff,nMon,nRef = self.__testEqual()
        self.debug("*** Kolmogorov-Smirnoff *** all non-empty bins=%d all equal non-empty bins=%d" % ( nbBins, nbBinsSame ) )

        pval = 1.0
        if nMon==0 and nRef==0:
            self.warn( "*** Kolmogorov-Smirnoff *** both '%s' histograms are empty!" % self.monPath )
        elif nbBinsSame==nbBins:
            self.debug( "*** Kolmogorov-Smirnoff *** both '%s' histograms are equal!" % self.monPath )
        elif not nMon:
            self.warn( "*** Kolmogorov-Smirnoff *** monitored '%s' histogram is empty!" % self.monPath )
            pval = 0.0
        elif not nRef:
            self.warn( "*** Kolmogorov-Smirnoff *** reference '%s' histogram is empty!" % self.refPath )
            pval = 0.0
        else:
            pval = self.monhist.KolmogorovTest( self.refhist, "D" if self.opts.verbosity <= 2 else "" )

        status = self.__getTestStatus( pval, pwarn, pfail )
        self.sumTable["KS"][status] = self.sumTable["KS"][status] + 1

        self.info( "*** Kolmogorov-Smirnoff *** (D) p-value=%4.3f status=%s" % ( pval, status ) )

        pValueNode = self.xmldoc.createElement( "pvalue" )
        pValueCData = self.xmldoc.createTextNode( "%4.3f" % pval )

        pValueNode.setAttribute( "test", "KS" )
        pValueNode.setAttribute( "status", status )

        counter = self.parent.getAttribute( "KS" + status )
        if ( not counter ): counter = "0"
        self.parent.setAttribute( "KS" + status, "%d" % ( int(counter) + 1 )  )

        pValueNode.appendChild( pValueCData )

        statNode.appendChild( pValueNode )
        return status

    ##  ** provide check if equal **
    # returns (nbBins,nbBinsSame,nbBinsDiff)
    #   nbBins - nb of bins not zero
    #   nbBinsSame - nb of bins not zero but equal
    #   nbBinsDiff - nb of bins not zero and different
    def __testEqual( self):
        nbBins = 0
        nbBinsSame = 0
        nMon = 0
        nRef = 0

        binX = self.monhist.GetNbinsX()
        binY = self.monhist.GetNbinsY()

        for i in range( binX+1 ):
            for j in range( binY + 1 ):
                mon = self.monhist.GetBinContent( i, j )
                ref = self.refhist.GetBinContent( i, j )
                if mon: nMon += 1
                if ref: nRef += 1
                if ( mon or ref ):
                    nbBins += 1
                    if ( mon == ref ):
                        nbBinsSame += 1
        return (nbBins,nbBinsSame,nbBins-nbBinsSame,nMon,nRef)

    ## perform "bin-by-bin" statistic test
    # @param self "Me, myself and Irene"
    # @param statNode <stat> element
    def __testBBB( self, statNode, pwarn, pfail ):

        nbBins,nbBinsSame,nbBinsDiff,nMon,nRef = self.__testEqual()
        self.debug("*** bin-by-bin *** all non-empty bins=%d all equal non-empty bins=%d" % ( nbBins, nbBinsSame ) )

        pval = 0.0
        if ( nbBins != 0 ):
            pval = float(nbBinsSame) / nbBins
            self.debug("*** bin-by-bin *** p-value=%4.3f" % pval )
        elif ( nbBinsSame == 0 ):
            pval = 1.0
            self.warn( "*** bin-by-bin *** both '%s' histograms are empty!" % self.monPath )
            self.debug( "*** bin-by-bin *** p-value=%4.3f" % pval )
        else:
            # AS: this part is never called?!
            self.warn( "*** bin-by-bin *** reference '%s' histogram is empty, while monitored has some entries" % self.refPath )
            self.debug( "*** bin-by-bin *** test failed" )

        status = self.__getTestStatus( pval, pwarn, pfail )
        self.info("*** bin-by-bin *** p-value=%4.3f status=%s" % ( pval, status ) )

        self.sumTable["bbb"][status] = self.sumTable["bbb"][status] + 1

        pValueNode = self.xmldoc.createElement( "pvalue" )
        pValueCData = self.xmldoc.createTextNode( "%4.3f" % pval )

        pValueNode.setAttribute( "test", "bbb" )
        pValueNode.setAttribute( "status", status )

        counter = self.parent.getAttribute( "bbb" + status )
        if ( not counter ): counter = "0"
        self.parent.setAttribute( "bbb" + status, "%d" % ( int(counter) + 1 )  )

        pValueNode.appendChild( pValueCData )

        statNode.appendChild( pValueNode )

        return status


    ## perform "diff(MeanY)" test for TProfile histos
    # @param self "Me, myself and Irene"
    # @param statNode <stat> element
    def __testMeanY( self, statNode, pwarn, pfail ):

        if not self.opts.meanYchi2:
            # this seems to be a stupidly specific test, but we keep it for compatibility.
            # Better to use another test for TProfile anyway.
            avgEffmon = self.mon.GetMean( 2 ) * 100
            avgEffref = self.ref.GetMean( 2 ) * 100

            self.debug("*** MeanY Test *** refMean=%4.3f and monMean=%4.3f" % ( avgEffref, avgEffmon ) )

            pval = abs(avgEffmon - avgEffref)

            status = self.__getMeanTestStatus( pval )   # override pwarn=1% pfail=5%
        else:
            avgEffmon = self.mon.GetMean     ( 2 )
            avgEffref = self.ref.GetMean     ( 2 )
            errEffmon = self.mon.GetMeanError( 2 )
            errEffref = self.ref.GetMeanError( 2 )

            err = math.sqrt( max(0.0,errEffmon)**2 + max(0.0,errEffref)**2 )

            if err > 0.0:
                stderr = abs((avgEffmon - avgEffref) / err)
                pval = 2.0*ROOT.Math.gaussian_cdf(-stderr,1)
            else:
                stderr = 0.0
                pval = 0.0

            self.debug("*** MeanY Test *** refMean=%4.3f and monMean=%4.3f, refErr=%4.3f and monErr=%4.3f, %.1f sigma" % ( avgEffref, avgEffmon, errEffref, errEffmon, stderr ) )
            status = self.__getTestStatus( pval, pwarn, pfail )

        self.info("*** Mean Test *** p-value=%4.3f status=%s" % ( pval, status ) )

        self.sumTable["meanY"][status] = self.sumTable["meanY"][status] + 1

        pValueNode = self.xmldoc.createElement( "pvalue" )
        pValueCData = self.xmldoc.createTextNode( "%4.3f" % pval )

        pValueNode.setAttribute( "test", "meanY" )
        pValueNode.setAttribute( "status", status )

        counter = self.parent.getAttribute( "meanY" + status )
        if ( not counter ): counter = "0"
        self.parent.setAttribute( "meanY" + status, "%d" % ( int(counter) + 1 )  )

        pValueNode.appendChild( pValueCData )

        statNode.appendChild( pValueNode )

        return status


    ## get test status for given pvalue
    # @param self "Me, myself and Irene"
    # @param pval p-value from test
    def __getTestStatus( self, pval, pwarn, pfail ):
        if ( ( pval < 0.0 or pval > 1.0 ) or
             ( pval <= pfail ) ): return "FAIL"
        if ( pval > pwarn ): return "OK"
        return "WARN"


    ## get test status for given pvalue
    # @param self "Me, myself and Irene"
    # @param pval p-value from test
    def __getMeanTestStatus( self, pval, pwarn=1.0, pfail=5.0 ):
        if ( pval <= pwarn ): return "OK"
        if ( pval <= pfail ): return "WARN"
        return "FAIL"


    ## make plots
    # @param self "Me, myself and Irene"
    def __makePlots( self ):
        if ( self.__plotter ):
            plotOpts = ";".join(p for p in (self.node.getAttribute( "plotopts" ), ";".join(self.opts.plotopts)) if p)
            status = self.__plotter.plot( self.node, self.mon, self.ref, plotOpts  )
            return status


    ## string representation of DCubeTester
    # @param self "Me, myself and Irene"
    def __str__( self ):
        out  = "*"*61 + "\n"
        out += "* RUN SUMMARY\n"
        out += "*"*61 + "\n"
        out += "* objects processed = %d\n" % self.__nbObjs
        out += "* STATISTICS TESTS TABLE\n"
        out += "* " + "-"*45 + "\n"
        out += "* | %-8s | %8s | %8s | %8s |\n" % ( "test", "OK", "WARN", "FAIL")
        self.allGOOD = 0
        self.allWARN = 0
        self.allFAIL = 0
        for key, value  in self.sumTable.items():
            nbGOOD = value["OK"]
            nbWARN = value["WARN"]
            nbFAIL = value["FAIL"]

            self.allGOOD += nbGOOD
            self.allWARN += nbWARN
            self.allFAIL += nbFAIL
            out += "* " + "-"*45+"\n"
            out += "* | %-8s | %8d | %8d | %8d |\n" % ( key, nbGOOD, nbWARN, nbFAIL )

        out += "* " + "-"*45+"\n"


        out += "* | %-8s | %8d | %8d | %8d |\n" % ( "all", self.allGOOD, self.allWARN, self.allFAIL )
        out += "* " + "-"*45+"\n"

        self.fracGOOD = 0.0
        self.fracWARN = 0.0
        self.fracFAIL = 0.0
        all = float( self.allGOOD + self.allWARN + self.allFAIL )
        if ( all ):
            self.fracGOOD = 100 * float( self.allGOOD ) / all
            self.fracWARN = 100 * float( self.allWARN ) / all
            self.fracFAIL = 100 * float( self.allFAIL ) / all
        out += "* | %%        |   %6.2f |   %6.2f |   %6.2f |\n" % ( self.fracGOOD, self.fracWARN, self.fracFAIL )
        out += "* " + "-"*45+"\n"

        out += "*"*61+"\n"
        self.nbErrors = sum( self.errors.values() )
        out += "* WARNINGS = %3d\n" % self.nbErrors
        i = 1
        for key, value in self.errors.items( ):
            sev, what = key.split(";")

            out += "* [%02d] %4s %-30s - occured %d " %  (i, sev, what, value )
            if ( value == 1 ): out += "time\n"
            else: out += "times\n"
            i += 1

        out += "*"*61+"\n"

        self.__status = ""

        if ( self.allFAIL != 0 ):
            self.__status = "FAIL"
        elif ( self.allWARN != 0 or self.nbErrors != 0 ):
            self.__status = "WARN"
        else:
            self.__status = "OK"

        out += "* OVERALL STATISTICS STATUS: %-4s\n" % self.__status

        out += "*"*61

        return out

    ## put summary to the logger and create summary node
    # @param self "Me, myself and Irene"
    # @return summary XML Element
    def summary( self ):

        # Print RUN SUMMARY to console (even if --verbosity=3), but still with "INFO" flag - it's not a warning.
        self.infoExtra( str(self).split("\n"), self.opts.verbosity-1 )

        summaryNode = self.xmldoc.createElement( "summary" )
        summaryNode.setAttribute( "status" , self.__status )
        summaryNode.setAttribute( "objs", "%d" % self.__nbObjs )
        summaryNode.setAttribute( "errors", "%d" % self.nbErrors  )

        testTable = self.xmldoc.createElement( "table" )
        testTable.appendChild( self.__sumTableRow( [ "test", "OK", "WARN", "FAIL" ] ) )
        for test, results in self.sumTable.items():
            testTable.appendChild( self.__sumTableRow( [ test,
                                                         results["OK"],
                                                         results["WARN"],
                                                         results["FAIL"] ] ) )

        testTable.appendChild( self.__sumTableRow( ["sum", self.allGOOD, self.allWARN, self.allFAIL ] ) )
        testTable.appendChild( self.__sumTableRow( ["fraction",
                                                    "%4.2f" % self.fracGOOD,
                                                    "%4.2f" % self.fracWARN,
                                                    "%4.2f" % self.fracFAIL ] ) )

        summaryNode.appendChild( testTable )

        errorsNode = self.xmldoc.createElement( "errors" )
        errorsNode.setAttribute( "nb", "%d" % self.nbErrors )

        for error,times in self.errors.items():
            error = error.replace(";", " ")
            errorNode = self.xmldoc.createElement( "error" )
            errorNode.setAttribute( "times", "%d" % times )
            errorNode.setAttribute( "what", error )
            errorsNode.appendChild( errorNode )

        summaryNode.appendChild( errorsNode )

        return summaryNode

    ## produce summary table row
    # @param self "Me, myself and Irene"
    def __sumTableRow( self, what ):
        row = self.xmldoc.createElement( "tr" )
        for item in what:
            cellNode = self.xmldoc.createElement("td")
            cellCData = self.xmldoc.createTextNode( str(item) )
            cellNode.appendChild( cellCData )
            row.appendChild( cellNode )
        return row

    ## return overall summary as string
    # @param self "Me, myself and Irene"
    def status( self ):
        return self.__status

    #check that values in histogram are not infinity or nan
    def __checkValues( self, values ):
        for val in values:
            if ( math.isinf(abs( val ) ) or math.isnan( val ) ) :
                return "BAD"
        return "OK"


    # convert plot to histogram type.
    def __2hist (self, plot, ref=[], graphName=None):
        if not plot: return plot
        if plot.InheritsFrom ("TGraph"):
            return self.__TGraph2hist (plot, ref, graphName)
        if plot.InheritsFrom ("TEfficiency"):   # we have already converted TEfficiency in caller, but keep this for completeness
            return self.__TEfficiency2TGraph (plot)
        # don't worry about TProfile. They are already a type of TH1.
        return plot

    # convert TGraph to list [[x,y,ey]...]
    def __TGraph2list (self, graph):
        if not graph: return []
        if not graph.InheritsFrom ("TGraph"): return []
        if not graph.GetN(): return []   # protect against crash in ROOT 6.22
        warnings.filterwarnings("ignore", category=FutureWarning)   # ignore internal PyROOT "buffer.SetSize(N) is deprecated" warning in ROOT 6.20.
        x = graph.GetX()
        y = graph.GetY()
        warnings.filterwarnings("default", category=FutureWarning)
        ey = [(ey if ey>=0 else None) for ey in [graph.GetErrorY(i) for i in range(len(x))]]
        if x and y and ey:
            return sorted (zip(x,y,ey), key=lambda p: p[0])
        else:
            return []

    # convert TGraph to TH1D. If a TGraphAsymmErrors, keep only symmetric y-errors.
    def __TGraph2hist (self, graph, ref=[], graphName=None):
        g = self.__TGraph2list (graph)
        if graphName is None: graphName= "%s::%s" % (graph.ClassName(), graph.GetName())
        nb = len(g)

        for refName,r in ref:
            # add bins in ref missing in g
            g2 = self.__TGraph2list (r)
            cnt1 = Counter([p[0] for p in g])
            cnt2 = Counter([p[0] for p in g2])
            if cnt1 == cnt2:
                self.debug("%s has the same %d points as %s graph" % (graphName, nb, refName))
            else:
                xtra = []
                for x2,c2 in cnt2.items():
                    c1 = cnt1.get(x2,0)
                    if c2 > c1:
                        add = [[x2,None,None]] * (c2-c1)
                        g += add
                        xtra += add
                if xtra:
                    self.warn("%s has %d points after adding %d missing points from %s graph at x=%s" % (graphName, len(g), len(g)-nb, refName,
                                                                                                         (",").join([str(x[0]) for x in xtra])))
                    g.sort (key=lambda p: p[0])
                    nb = len(g)

        even = True
        if len(g) >= 3:
            # check for missing bins if equal spacing
            xwid = [b[0]-a[0] for a,b in zip(g,g[1:])]
            xwmin = min(xwid)
            if xwmin > 0.0 and max([x % xwmin for x in xwid]) / xwmin < 1e-10:
                xtra = []
                if max(xwid)/xwmin-1.0 >= 1e-10:
                    for a,b in zip(g,g[1:]):
                        xw = b[0]-a[0]
                        if xw == xwmin: continue
                        nbx = int (xw/xwmin + 0.5)
                        if nbx>=2:
                          add = [[a[0]+i*xwmin,None,None] for i in range(1,nbx)]
                          g += add
                          xtra += add
                if xtra:
                    self.warn("%s has %d evenly %g-spaced points after adding missing %d at x=%s" % (graphName, len(g), xwmin, len(g)-nb,
                                                                                                     (",").join([str(x[0]) for x in xtra])))
                    g.sort (key=lambda p: p[0])
                    nb = len(g)
                else:
                    self.info("%s has %d evenly %g-spaced points" % (graphName, nb, xwmin))
            else:
                self.info("%s has %d iregularly-spaced points" % (graphName, nb))
                even = False

        if even:
            if nb >= 2:
                xlo, xhi = 0.5*(3.0*g[0][0]-g[1][0]), 0.5*(3.0*g[nb-1][0]-g[nb-2][0])
            elif nb == 1:
                xlo = g[0][0] - 0.5
                xhi = xlo + 1.0
            else:
                xlo, xhi = 0.0, 1.0
            h = ROOT.TH1D (graph.GetName(), graph.GetTitle(), max(nb,1), xlo, xhi)
        else:
            xbins = [2*g[0][0]-g[1][0]] + [p[0] for p in g] + [2*g[nb-1][0]-g[nb-2][0]]
            xbins = [0.5*(a+b) for a,b in zip(xbins,xbins[1:])]
            h = ROOT.TH1D (graph.GetName(), graph.GetTitle(), nb, array('d',xbins))
        h.SetDirectory(0)
        hg = graph.GetHistogram()
        if hg:
          h.GetXaxis().SetTitle (graph.GetXaxis().GetTitle())
          h.GetYaxis().SetTitle (graph.GetYaxis().GetTitle())

        for i,gv in enumerate(g):
            if gv[1] is not None: h.SetBinContent (i+1, gv[1])
            if gv[2] is not None: h.SetBinError   (i+1, gv[2])

        return h

    # Convert TEfficiency to TGraph (1D) or TH2F (2D)
    def __TEfficiency2TGraph (self, eff):
        if not eff: return eff
        if eff.GetDimension() == 2:
            hist = eff.CreateHistogram()
            hist.SetName (eff.GetName())
            return hist
        else:
            graph = eff.CreateGraph("AP")
            graph.SetNameTitle (eff.GetName(), eff.GetTitle())
            # remove x-error bars
            for i in range( graph.GetN() ):
                graph.SetPointEXlow( i, 0.0 )
                graph.SetPointEXhigh( i, 0.0 )
            return graph

##
# @class test_DCubeTester
# @author Krzysztof Daniel Ciba (Krzysztof.Ciba@NOSPAMgmail.com)
# @brief test case for DCubeTester class
class test_DCubeTester( unittest.TestCase ):

    ## setup test cases
    # @param self "Me, myself and Irene"
    def setUp( self ):
        pass

    ## ctor
    # @param self "Me, myself and Irene"
    def test_01_ctor( self ):
        pass


## test case execution
if __name__ == "__main__":
    pass


