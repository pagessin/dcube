#!/bin/bash
# Compare InDetSLHC_Example 10 GeV muons against 100 GeV muons using art-dcube wrapper.
# This produces many plots, some with warnings (eg. empty).
# We expect some interesting differences, so will get comparison failures.

dir=$(dirname "$0")
name=$(basename "$0" ".sh")
dcubedir=$(dirname "$dir")
reldir=$(basename "$dir")
tmp="${TMPDIR:-/tmp}/dcube-$name.$$"
rootver="views LCG_96b x86_64-centos7-gcc8-opt"
keep=0 atlas=0 show=:
while [ $# -gt 0 ]; do  # must come before dcube.py options
  case "$1" in
    -k) keep=1; shift;;
    -v) show=""; shift;;
    -a) atlas=1; shift;;
    -r) rootver="$2"; shift; shift;;
    *) break;;
  esac
done

function tidy()
{
  sed -e 's=plots/[^"]*\.png=plots/X.png=g' -e '{$!{N;N;s=<date>\s*.*\s*</date>==;t;P;D}}' "$@"
}

function tidylog()
{
  sed -e 's= -x [^ ]*/art-test. = =' "$@"
}

if [ $atlas -ne 0 ]; then
  test -n "$ATLAS_DCUBE_DIR" && dcubedir="$ATLAS_DCUBE_DIR/DCubeClient";
else
  # Set up ROOT. Version 6.18 has a different XML formatting (vs newer 6.20), so we simplify the comparison by requiring this version here.
  export ATLAS_LOCAL_ROOT_BASE="/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase"
  . "$ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh" -q
  . "$ATLAS_LOCAL_ROOT_BASE/packageSetups/localSetup.sh" -q "$rootver"
fi

$show set -x
# Don't include current asetup in result xml.
unset AtlasBuildBranch CMTCONFIG AtlasReleaseType AtlasVersion AtlasProject
# cd to dcube directory so paths in log are the same as in the reference
cd "$dcubedir"

mkdir -p "$tmp"
# The config file was copied from /cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/InDetSLHC_Example/dcube/config/ITk_IDPVM.xml .
"./bin/art-dcube" \
  "InclinedAlternative" \
  "/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/InDetSLHC_Example/ReferenceHistograms/physval.ATLAS-P2-ITK-17-04-02_single_mu_Pt10_digi.root" \
  "./$reldir/${name}-config.xml" \
  "/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/InDetSLHC_Example/ReferenceHistograms/physval.ATLAS-P2-ITK-17-04-02_single_mu_Pt100_digi.root" \
  "$tmp/$name" \
  "$@" \
  &> "$tmp/$name.log"

test -s "$tmp/$name.log"       || exit 98
diff <(tidylog "$dir/$name.log") <(tidylog "$tmp/$name.log")
stat=$?
test -s "$tmp/$name/dcube.xml" || exit 99
diff <(tidy "$dir/${name}-result.xml") <(tidy "$tmp/$name/dcube.xml") || \
stat=$?

if [ $keep -eq 0 ]; then
  rm -rf "$tmp"
else
  echo "$(basename "$0"): output saved in $tmp" >&2
fi
exit $stat
