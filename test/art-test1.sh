#!/bin/bash
# Compare InDetPerformanceRTT Z->mumu+pileup against Z-mumu without pileup using dcube.py.
# We expect some interesting differences, so will get comparison failures.
dir=$(dirname "$0")
name=$(basename "$0" ".sh")
dcubedir=$(dirname "$dir")
tmp="${TMPDIR:-/tmp}/dcube-$name.$$"
rootver="views LCG_97_ATLAS_1 x86_64-centos7-gcc8-opt"
keep=0 atlas=0 show=:
while [ $# -gt 0 ]; do  # must come before dcube.py options
  case "$1" in
    -k) keep=1; shift;;
    -v) show=""; shift;;
    -a) atlas=1; shift;;
    -r) rootver="$2"; shift; shift;;
    *) break;;
  esac
done

function tidy()
{
  sed -e 's=plots/[^"]*\.png=plots/X.png=g' -e '{$!{N;N;s=<date>\s*.*\s*</date>==;t;P;D}}' "$@"
}

if [ $atlas -ne 0 ]; then
  test -n "$ATLAS_DCUBE_DIR" && dcubedir="$ATLAS_DCUBE_DIR/DCubeClient";
else
  # Set up ROOT. Version 6.20 has a different XML formatting (vs 6.18), so we simplify the comparison by requiring this version here.
  export ATLAS_LOCAL_ROOT_BASE="/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase"
  . "$ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh" -q
  . "$ATLAS_LOCAL_ROOT_BASE/packageSetups/localSetup.sh" -q "$rootver"
fi

$show set -x
mkdir -p "$tmp"
# The -c config file was copied from /cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/InDetPerformanceRTT/dcube/config/ZMuMu_DCubeConfig.xml
"$dcubedir/python/dcube.py" -p \
   --branch="*" --cmtconfig="*" --install="*" --jobId="*" --project="*" \
   -x "$tmp/$name" \
   -c "$dir/${name}-config.xml" \
   -r "/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/InDetPerformanceRTT/ReferenceHistograms/InDetStandardPlots-ZMuMu.root" \
   "$@" \
      "/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/InDetPerformanceRTT/ReferenceHistograms/InDetStandardPlots-ZMuMuPileup.root" \
   &> "$tmp/$name.log"

test -s "$tmp/$name.log"       || exit 98
diff "$dir/$name.log" "$tmp/$name.log"
stat=$?
test -s "$tmp/$name/dcube.xml" || exit 99
diff <(tidy "$dir/${name}-result.xml") <(tidy "$tmp/$name/dcube.xml") || \
stat=$?

if [ $keep -eq 0 ]; then
  rm -rf "$tmp"
else
  echo "$(basename "$0"): output saved in $tmp" >&2
fi
exit $stat
